#include <Preferences.h>
#include <DHTesp.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <WiFi.h>
#include "time.h"
#include "FS.h"
#include "SD.h"
#include <virtuabotixRTC.h>
#define DHTpin 4
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define BuzzerPIN 2

String ssid = "Zero's WiFi";
String pass = "qwertyuiop99";
String fechahora = "";
const char* ntpServer = "pool.ntp.org";
long  gmtOffset_sec = -21600;    //3600 por hora, estamos en GMT-6 (mas una hora del horario de verano) => 3600*-6
const int   daylightOffset_sec = 3600;
Preferences config;
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
DHTesp sensor;
int temp, hum;
int intSuperior[] = { 1500,1500,1500,1500 };
int intInferior[] = { -20,-20,-20,-20 };
String textoAlarma[] = { "","","","" };
String comando;
virtuabotixRTC myRTC(16, 27, 14);


void writeFile(fs::FS& fs, const char* path, const char* message) {
    Serial.printf("Escribiendo archivo: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if (!file) {
        Serial.println("Fallo al abrir el archivo para escribir");
        return;
    }
    if (file.print(message)) {
        Serial.println("Archivo escrito");
    }
    else {
        Serial.println("Fallo en escritura");
    }
    file.close();
}

void appendFile(fs::FS& fs, const char* path, const char* message) {
    Serial.printf("Agregando al archivo: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if (!file) {
        Serial.println("Error al abrir el archivo para agregar");
        return;
    }
    if (file.print(message)) {
        Serial.println("Texto agregado");
    }
    else {
        Serial.println("Fallo al agregar");
    }
    file.close();
}


void ConectarWiFi() {
    WiFi.begin(ssid.c_str(), pass.c_str());
    Serial.print("Conectando a WiFi");
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.println(" Ok!!");
}

void FechaHora() {
    myRTC.updateTime();
    fechahora = String(Corrige(myRTC.dayofmonth) + "/" + Corrige(myRTC.month)
        + "/" + String(myRTC.year) + "   " + Corrige(myRTC.hours) + ":"
        + Corrige(myRTC.minutes) + ":" + Corrige(myRTC.seconds));
    Serial.println(fechahora);
}

void ObtenerFechaHora() {
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        Serial.println("Error en obtener la fecha y hora");
        return;
    }
    fechahora = String(Corrige(timeinfo.tm_mday) + "/" + Corrige(timeinfo.tm_mon + 1) + "/" +
        String(timeinfo.tm_year + 1900) + "   " + Corrige(timeinfo.tm_hour) + ":" + Corrige(timeinfo.tm_min)
        + ":" + Corrige(timeinfo.tm_sec));
    Serial.println(fechahora);
    myRTC.setDS1302Time(timeinfo.tm_sec, timeinfo.tm_min, timeinfo.tm_hour,
        timeinfo.tm_wday, timeinfo.tm_mday, timeinfo.tm_mon + 1,
        timeinfo.tm_year + 1900);
    // SS, MM, HH, DW, DD, MM, YYYY
}

String Corrige(int n) {
    if (n < 10) {
        return "0" + String(n);
    }
    else {
        return String(n);
    }
}

void LeerConfiguracion() {
    config.begin("config", false);
    intSuperior[0] = config.getInt("is0", 1500);
    intInferior[0] = config.getInt("ii0", -20);
    intSuperior[1] = config.getInt("is1", 1500);
    intInferior[1] = config.getInt("ii1", -20);
    intSuperior[2] = config.getInt("is2", 1500);
    intInferior[2] = config.getInt("ii2", -20);
    intSuperior[3] = config.getInt("is3", 1500);
    intInferior[3] = config.getInt("ii3", -20);
    textoAlarma[0] = config.getString("ta0", "");
    textoAlarma[1] = config.getString("ta1", "");
    textoAlarma[2] = config.getString("ta2", "");
    textoAlarma[3] = config.getString("ta3", "");
}

void setup() {
    //config.clear();
    Serial.begin(9600);
    LeerConfiguracion();
    ConectarWiFi();
    pinMode(BuzzerPIN, OUTPUT);
    sensor.setup(DHTpin, DHTesp::DHT11);
    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        Serial.println(F("SSD1306 allocation failed"));
        for (;;);
    }
    if (!SD.begin()) {
        Serial.println("Error al cargar la micro SD");
        return;
    }
    ObtenerFechaHora();
    writeFile(SD, "/datalog_ESP32.txt", "Inicio de archivo \r\n");
    delay(2000);
    display.setTextColor(WHITE);
    display.clearDisplay();
}

void Bip() {
    digitalWrite(BuzzerPIN, HIGH);
    delay(200);
    digitalWrite(BuzzerPIN, LOW);
}

void Escribir(String texto, int x, int y, int size) {
    display.setTextSize(size);
    display.setCursor(x, y);
    display.println(texto);
    //display.display();
}

void Sensar() {
    TempAndHumidity valores = sensor.getTempAndHumidity();
    temp = valores.temperature;
    hum = valores.humidity;
    Serial.println("T=" + String(temp) + " H=" + String(hum));
}

int HayAlarma() {
    for (int i = 0; i < 4; i++) {
        if (intSuperior[i] < 100) {
            //esta configurada la alarma
            if (temp<intInferior[i] || temp>intSuperior[i]) {
                return i;
            }
        }
    }
}

char* ConvertirCadenaToCharArray(String texto) {
    char cadena[200];
    texto.toCharArray(cadena, 200);
    return cadena;
}

void ValidarComando() {
    /* 01234
     * ii0=<num>
     * is0=<num>
     * ta0="cadena"
     */

    if (comando[0] == 'i')
    {
        if (comando[1] == 'i') {
            int pos = String(comando[2]).toInt();
            if (pos >= 0 && pos <= 3) {
                intInferior[pos] = comando.substring(4).toInt();
                config.putInt(ConvertirCadenaToCharArray(comando.substring(0, 3)), comando.substring(4).toInt());
            }
        }
        if (comando[1] == 's') {
            int pos = String(comando[2]).toInt();
            if (pos >= 0 && pos <= 4) {
                intSuperior[pos] = comando.substring(4).toInt();
                config.putInt(ConvertirCadenaToCharArray(comando.substring(0, 3)), comando.substring(4).toInt());
            }
        }
    }

    if (comando[0] == 't') {
        if (comando[1] == 'a') {
            int pos = String(comando[2]).toInt();
            if (pos >= 0 && pos <= 3) {
                textoAlarma[pos] = comando.substring(4);
                config.putString(ConvertirCadenaToCharArray(comando.substring(0, 3)), comando.substring(4));
            }
        }
    }
}

void loop() {
    if (Serial.available()) {
        comando = Serial.readString();
        comando = comando.substring(0, comando.length() - 2);
        //Serial.println("[" + comando + "]");
        ValidarComando();
        comando = "";
    }
    FechaHora();
    Sensar();
    Escribir("Termohigrometro IoT", 5, 10, 1);
    int alarma = HayAlarma();
    Escribir(fechahora, 0, 20, 1);
    /*
    if(alarma>=0){
      Escribir(textoAlarma[alarma],0,20,1);
      Bip();
    }else{
      Escribir("   Temp       Hum",0,20,1);
    }
    */
    if (temp == 2147483647) {
        Escribir("NA", 5, 30, 3);
        Escribir("NA", 70, 30, 3);
    }
    else {
        Escribir(String(temp), 5, 30, 3);
        Escribir(String(hum), 70, 30, 3);
    }
    Escribir("Online", 0, 55, 1);
    Escribir("MicroSD ON", 50, 55, 1);
    Escribir("o", 40, 35, 1);
    Escribir("C", 47, 35, 2);
    Escribir("%", 108, 35, 2);
    display.display();
    appendFile(SD, "/datalog_ESP32.txt", String(fechahora + "\t" + String(temp)
        + "\t" + String(hum) + "\r\n").c_str());
    delay(2500);
    display.clearDisplay();
}